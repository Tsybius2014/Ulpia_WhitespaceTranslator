#Ulpia_WhitespaceTranslator

Ulpia是一个Whitespace语言源码转换工具。Whitespace语言的源码中只包含三种字符：Space（空格）、Tab（制表符）和Linefeed（换行），而这三种字符都是不可见的字符，这会给Whitespace源码的编写造成很大困难。一个可行的方案，是用三组其他的字符串，替代Whitespace源码中的不可见字符，通过组合这三组可见的字符串，来编写Whitespace的程序逻辑，程序编写完毕后，再将写好的文本转换成Whitespace编译器可以识别的程序源码。Ulpia就是用来执行这个任务的。


Ulpia的使用方法：
```python
#将源码source.txt转换为可视文本保存到temp.txt
python Ulpia_WhitespaceTranslator.py -e source.txt temp.txt
#将源码temp.txt转换为Whitespace编译器可识别的源码并保存到source.txt
python Ulpia_WhitespaceTranslator.py -d temp.txt source.txt
```